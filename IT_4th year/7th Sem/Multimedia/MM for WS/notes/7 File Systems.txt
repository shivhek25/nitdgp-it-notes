File Systems

-It is the most visible part of the OS. Most programs read/write files.
-The program code & user data are stored in the files.
-Files are stored in secondary storage.
-Traditional File Systems (FS) have information in the form of sources, objects, libraries, executable programs, numeric data, text, payroll records etc.
-In MM FS, it also includes digital audio & video with related real time read & write demands.
-The FS provides access & control functions for storage & retrieval of files.
-From the user’s point of view, it is important that FS supports file structure & organization.

-Traditional FS
	-There are two main goals:
		-A comfortable user interface for file access
		-To make efficient use of storage media.
	
	-The file structure is of two types:
		-Sequential
		-Non-sequential
	
	-Compare & contrast seq and non seq fs
		-In sequential storage, file placement is contiguous whereas in the later form, file placement is non contiguous.
		-In sequential storage, there is a file descriptor at the beginning of each file & in some systems, it is repeated at the end of files.
		It is the only way of file storage in tapes and can also be used in disks.
		Main advantage is efficiency for sequential access & direct access – disk access time is minimized for read & write for sequential access.
		But in systems with heavy insertion, deletion & file modification, sequential storage is disadvantageous.
		Secondary storage is split & fragmented & files may be copied so that all the files are adjacently located without any “holes” between them.

	-Disk management
		-Disk access is slow & costly.
		-In traditional systems, a technique to reduce disk access are block caches where certain blocks are kept in memory for future access for read & write.
		Performance increases due to shorter memory access time.
		-Another way to enhance performance is to reduce disk arm motion.
		Blocks to be accessed sequentially are placed on one cylinder together.
		To refine this method, rotational positioning is done in same cylinder but in an interleaved fashion on the same cylinder.

	-Disk scheduling
		-Strictly sequential storage devices have no scheduling problem.
		-But for random access storage devices, every file operation requires a movement of read/write head.
		-This is called “SEEK” operation which is maximum time consuming which is about 250 ms for CDs.
		-Actual read/write time for a disk block is:
			-Seek-time: required for head movement.
			-Latency time or rotational delay period which is time during which the transfer cannot proceed until the right block/sector rotates under read/write head.
			-The actual data transfer time needed for the data to copy from disk into main memory.
		-Usually seek time is the largest factor of actual transfer time.
		-Most scheduling algorithms are applied to minimize seek time. Common scheduling algorithms are:
			-FCFS (First Come First Serve)
				-With this algo, disk driver accepts requests one at a time & serves them in incoming order. Easy to program but not optimum w.r.t. head movement.
			-SSTF (Shortest Seek Time First)
				-When data transfer is requested, at a particular time, SSTF selects among all requests the one with minimum seek time from the current head position.
				-So, the head is moved to the closest track in the request queue.
				-Some request may not be served at all and so there can be a starvation problem.
				-Request targets in the middle positions will get immediate service at the expense of request in the innermost & outermost disk areas.
			-SCAN
				-Like SSTF, SCAN orders requests to minimize seek time.
				-In contrast to SSTF, it takes direction of current disk movement into account.
				-It first serves all the requests in one direction till it does not have any pending requests in that direction.
			-C-SCAN
				-C-Scan also moves the head in one direction, but it offers fairer service with more uniform waiting times.
				-It does not alter direction as SCAN (SCAN is unidirectional).
				-Instead, it scans in cycles, always increasing or decreasing with one idle head movement from one edge to the other between two consecutive scans.
				-Performance of C-SCAN is somewhat less than SCAN.

-MM FS
	-Continuous media data i.e. MM data are different from discrete data in the following ways:
		-Realtime characteristics
			-Retrieval, computation & presentation of continuous media data is time dependent.
			-So, algos for storage & retrieval of such data must consider time constraints.
			-Additional buffers must be provided to smooth out the data stream i.e. to avoid jitter.
		-File Size
			-Compared to text & audio, MM files require lot of storage space requirements.
			-So the FS must organize the data on disks so that the limited storage is used efficiently.
		-Multiple data streams
			-It should support different media at the same time.
			-So, all of them should share resources.
			-There are different ways to support continuous media in FS.
			-There are two approaches:
				-The organization of files on disk remains the same. The real-time support is provided through a special disk scheduling algos & sufficient buffer is used to avoid jitter.
				-The organization of audio & video files on disk is optimized for there use in MM systems.

	-Storage devices
		-The storage sub-system is a major component of FS.
		-In continuous media, the storage requirements are very high & so conventional storage systems are not sufficient.
		-CD-ROMS are used along with disks of high capacity.
		-The classification of disks is done in two ways:
			-On how data is stored on them.
				These are rewriteable disks (both optical & magnetic), WORM (write once) disks & read only disks like CD-ROMs.
			-On how data is recorded.
				-It is distinguished between magnetic & optical disks.
				-Main difference is on access time & track capacity.
				-The seek time in magnetic disk is above 10 ms whereas in optical disk lower bound is 200 ms.
				-Magnetic disks have constant rotation speed (CAV : Constant angular velocity) so when density varies, storage capacity is same on the inner & outer tracks whereas optical disks have varying rotation speed (CLV: Constant linear velocity), so storage density is same on the whole disk.
	
	-File structure and placement on disk
			-Main aim in conventional FS
				-To make efficient use of storage capacity i.e. to reduce fragmentation & allows arbitrary deletion & extension of files.
			-Main aim in MMFS
				-On constant & timely retrieval of data.
			-Fragmentation
				-Internal
					-When blocks of data are not used up fully.
					-Use of larger blocks lead to greater wastage.
				-External
					-When files are stored contiguously.
					-After a file is deleted, the gap can be filled with a file of smaller or equal size.
					-So, when small fractions between files lay unused or unfilled, it is wasted.
					-In MM FS,
						-For each data stream, enough buffer needs to be provided, 
						-Special disk scheduling algos optimized for real time storage & retrieval are used, 
						-External fragmentation is avoided & the same data can be used by several streams via references.
						-Using only one stream can be helpful as the same block may be accessed again & again (a phrase in a sonata may be repeated), but the seek time is very large & during playback for every operation, large buffers must be provided to smoothen jitter during retrieval phase even with optimized disk scheduling algos.
						-Long initial delays at retrieval of continuous media data are encountered.
						-Another problem is restricted transfer rate. The transfer rate is not enough for simultaneous retrieval of multiple video streams.
						For ex; even with upcoming disk arrays with more than 100 parallel heads, with projected seek & latency time <10 mS & block size = 4KB & a transfer rate of 0.32 GB/sec, simultaneous retrieval of 4 or more MPEG – 2 videos compressed in HDTV quality is not possible as it requires transfer rate upto 100 Mbits/sec.
						In continuous media streams, data is recorded & played back sequentially.
						Audio & video streams recorded at the same time must be played back at the same time. So, large data blocks are stored contiguously.
						The disadvantage is external fragmentation & copying overhead during insertion & deletion.
						To avoid this, media continuity between the gaps must be maintained.
						If M is the size of blocks & G is the size of gap between them, continuity is maintain if the time needed to skip over the gap & read the next media block does not exceed the duration of playback Tplay(s) such that
						Tplay(s)>=   M(sectors)+G(sectors)
									----------------------
						               rdt (sectors/sec)
						-Scattered storage can also be used.
						With interleaved placement, all nth block of each stream are on close physical proximity on disk.
						A contiguous interleaved placement as well as scattered & interleaved placement is possible.
						With interleaved data stream it is easy to handle synchronization but insertion & deletion of single parts of data streams become complicated.

	-Disk scheduling algos
		-Earliest Deadline first (EDF)
			-For CPU scheduling
			-For file systems also.
			-The block of the stream with nearest deadline to be read first.
			-Disadv
				-Excessive seek time.
				-Very poor throughput.
			-Used as a pre-emptive scheduling scheme but cost of pre-emption is very high at least one disk seek.
			-Used with other cost effective algos.
		-SCAN-EDF
			-Combination of seek optimization for SCAN & real-time guarantees of EDF.
			Request with earliest deadline is served first like EDF, among same deadline, request that is first according to SCAN direction is served first; among examining requests, same procedure is repeated until no dead line is left.
			-Can be easily implemented by modifying EDF slightly.
				-If Di is deadline of a task & Ni be the track position, deadline can be modified to be Di + f(Ni).
				-Thus the deadline is deferred. The fn f () converts the track number of i to a small perturbation of the deadline.
				-The f must be small so that Di + f(Ni) <= Dj+f(Nj) for all Di<=Dj.
				-For f(Ni) the fn is chosen as: f(Ni) = Ni/Nmax. where Nmax is maximum track no. on the disk.
			-We enhance the mechanism by proposing a more accurate perturbation of the deadline which takes into account the actual position of the head (N). The position is measured in terms of numbers & current direction of head movement.
				-If the head moves toward Nmax i.e. upward, then
					-for all blocks Ni, located between actual position N & Nmax, the perturbation of deadline is f(Ni) = Ni – N for all Ni>=N
						              ----------
						                 Nmax
				    -for all blocks Ni located between the actual position & first block (no. zero): 
					f(Ni) = Nmax – Ni  for all Ni <N
				           -----------
				              Nmax
				-If head moves downwards towards the first blocks, then
					-For all blocks located between actual position & Nmax, 
					f(Ni) = Ni     for all Ni>=N
					       ----
					       Nmax
					-for all blocks Ni located between the actual position & first block (no. zero): 
					f(Ni) = N – Ni            for all Ni <=N
							------
					         Nmax
		
		-Group sweeping schedule
			-Requests are served in a round robin manner in cycles.
			
			-To reduce the disk arm movements, set of n streams are divided to g groups.
			Groups are served in fixed order.
			Individual streams within a group are served according to SCAN, therefore it is not fixed at which time or order, the individual streams within a group shall be served.
			In one cycle, a specific stream may be the first to be served & in the next cycle, it may be the last to be served.
			
			-A smoothing buffer sized according to the cycle time & data rate of stream assures continuity.
			If Scan scheduling is applied to all streams of a cycle without any grouping, play-out of a stream cannot be started until the end of the cycles of its first retrieval (where all requests are served once) because the next service may be in the last slot of following cycle.
			As the data must be buffered in GSS, the play-out can be started at the end of the group in which the first retrieval takes place while SCAN requires buffers for all streams.
			In GSS, the buffers may be reused for each group.

			-Further optimization can be done.
			Each stream is served in one cycle.
			GSS is a trade-off between optimization of buffer space & arm movements.
			There is a joint deadline and each group of stream is assigned a deadline.
			The deadline is earliest one out of deadlines of all streams in respective groups.
			Streams are grouped in such a way that all of them comprise similar deadlines.

		-Mixed Strategy
			-It is based on shortest seek (also called greedy strategy ) and balanced strategy.
			-Every time data are retrieved from disk, they are transferred to buffer memory allocated for each stream.
			-Goal of the scheduling algo is
				-To maximize transfer efficiency by minimizing seek time & latency.
				-To serve process requirements with a limited buffer space.
			-With shortest seek, the first goal is served, i.e. the data block which is closest is served first.
			-The balanced strategy chooses the process that has least amount of buffered data for services as this process is likely to run out of data.
			-The crucial part of this algo is decision of which of these two strategies must be applied – shortest seek or balanced strategy.
			-For employment of shortest seek, two criteria must be fulfilled:
				-Number of buffers for all processes should be balanced i.e. all processes should nearly have the same amount of buffered data.
				-Overall required bandwidth should be sufficient for the number of active processes so that none f them will try to immediately read out of empty buffers.