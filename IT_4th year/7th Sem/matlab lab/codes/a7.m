clc;
a=[20,22,17,4;24,37,9,7;32,37,20,15];
disp(a);
b = zeros(3,4);
c = a;
supply = [120;70;50];
demand = [60,40,30,110];
cost = 0;
while(max(max(supply),max(demand))~=0)
    penalty_row = zeros(size(a,1),1);
    penalty_col = zeros(1,size(a,2));
    for i=1:size(a,1)
        if(min(setdiff(a(i,:),min(a(i,:))))~=inf)
            penalty_row(i) = min(setdiff(a(i,:),min(a(i,:))))-min(a(i,:));
        end
    end
    for i=1:size(a,2)
        if(min(setdiff(a(:,i),min(a(:,i))))~=inf)
            penalty_col(i) = min(setdiff(a(:,i),min(a(:,i))))-min(a(:,i));
        end
    end
    max_r=max(penalty_row);
    max_c=max(penalty_col);
    if(max_r>max_c)
        index = find(penalty_row==max_r,1);
        min_el = min(a(index,:));
        in = find(a(index,:)==min_el);
        sub = min(demand(in),supply(index));
        b(index,in)= sub;
        demand(in) = demand(in)-sub;
        supply(index) = supply(index)-sub;
        if(supply(index)==0)
            a(index,:)=inf;
        else
            a(:,in)=inf;
        end
    else
        index = find(penalty_col==max_c,1);
        min_el = min(a(:,index));
        in = find(a(:,index)==min_el);
        sub = min(demand(index),supply(in));
        b(in,index)= sub;
        demand(index) = demand(index)-sub;
        supply(in) = supply(in)-sub;
        if(demand(index)==0)
            a(:,index)=inf;
        else
            a(in,:)=inf;
        end
    end
    cost = cost + min_el*sub; 
end
disp(b);
disp(cost);
opt=size(find(b~=0),1)==size(a,1)+size(a,2)-1;
disp(opt);
for i=1:size(b,1)
    for j=1:size(b,2)
        if(b(i,j)~=0)
            for k=1:size(b,1)
                if(b(k,j)~=0)
                    for l=1:size(b,2)
                        if(b(i,l)~=0) && (b(k,l)~=0)
                            
                        end
                    end
                end
            end
        end
    end    
end