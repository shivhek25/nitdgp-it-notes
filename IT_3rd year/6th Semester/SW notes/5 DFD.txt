Data Flow Diagram
	-Basically used to show flow of data through system
	-Circle:	Process
	-Arrows:	Data flow
	-Rectangle: Source which is either a net originator or consumer of data
	-Labelled straight line:	External files
	-* between data flows(represents AND operation):	Multiple data flows
	-DFDs v flowchart
		-DFD:flow of data	Flowchart:flow of control
		-DFD:Does not represent procedural info;Ex:consideration of loops, decisions must be ignored
		-DFD:Designer designs major transforms in the flow of the i/p to o/p not the how part
		-DFD:Error messages not shown
	-Leveled DFDs
		-Many systems are too large for single dfd to represent entire data processing clearly
		-So, it becomes necessary that some decomposition and abstraction mechanisms be used for such large systems
		-DFDs can be hierarchically organized which helps in progressive partitioning and analyzing of large systems. Such dfds are called “Leveled DFD set”
		-How to draw
			-A leveled DFD set has a starting dfd, which is a very abstract representation of the system identifying the major inputs ,outputs and the major processes in the system.
			-Then each process is refined and a dfd is shown for the corresponding refinements.
			-In other words, a bubble in a dfd is expanded into another dfd during refinement.
			-For the hierarchy to be consistent, it is necessary that the net inputs and outputs of a dfd for a process are the same as the inputs and the outputs of the process in the higher level dfd.
			-This refinement stops if each bubble is considered to be “atomic” ; in that case each bubble can be easily specified or understood.
			-The top level dfd is sometimes called the “context-diagram”
		-Data Dictionary
			-Data dictionary is almost a data structure that keeps track of information about data flows.
			-Data dictionary states precisely the structure of each data flow in the dfd
Jackson's Structured Programming
	-Systematic technique for mapping the structure of any problem into a program structure
	-Steps for construction
		-To draw the “data structure diagram” for each data set
		-Form a “program structure diagram” from the data structure diagram
		-List all “operations” ,”functional components” and “conditions”