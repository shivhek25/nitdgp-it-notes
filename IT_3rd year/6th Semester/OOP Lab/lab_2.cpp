#include<bits/stdc++.h>
#define ll long long int
using namespace std;
class Relatives
{
private:
    string Name;
    string Address;
public:
    void input()
    {
        cout << "Enter name and address:\n";
        getline(cin,Name);
        getline(cin,Address);
    }
    void display()
    {
        cout << Name << "\n" << Address << "\n";
    }
};
int main()
{
    Relatives relative;
    while(1)
    {
        relative.input();
        relative.display();
    }
    return 0;
}
