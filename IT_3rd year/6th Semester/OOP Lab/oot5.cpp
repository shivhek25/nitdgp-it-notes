#include<iostream>
using namespace std;

class post
{
    float weight;
    float cost;
public:
    post()
    {
        weight=0;
        cost=0;
    }
    post(float w)
    {
        weight=w;
        cost=0;
    }
    friend float calculate(post p);
    void display()
    {
        cout<<"Packet weight : "<<weight<<endl;
        cout<<"Packet cost : "<<cost<<endl;
    }
};


float calculate(post p)
{
    if(p.weight>15)
    {
        p.weight=p.weight-15;
        p.cost=2+(int)((p.weight+9)/10);
    }
    else
        p.cost=2;
    return p.cost;
}



int main()
{
    int m;
    cout<<"Enter number of packets: ";
    cin>>m;
    float tot_cost=0;
    cout<<"Enter the weights of each packet:-"<<endl;
    for(int i=0;i<m;i++)
    {
        float x;
        cin>>x;
        post p(x);

        //cout<<calculate(p);

        tot_cost=tot_cost+calculate(p);

    }
    cout<<"The total cost is : "<<tot_cost<<endl;

    return 0;
}
