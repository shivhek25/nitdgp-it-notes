#include<iostream>
using namespace std;

class box
{
    double a,b,c;
    double vol;
public:

     box()
     {
         a=0.0;
         b=0.0;
         c=0.0;
        vol=0.0;
     }

     box(double x,double y,double z)
     {
         a=x;
         b=y;
         c=z;
         vol=0.0;
     }

     void volume()
     {
         vol= a*b*c;

     }

     void dis()
     {
         cout<<"Length of side 1 : "<<a<<endl;
         cout<<"Length of side 2 : "<<b<<endl;
         cout<<"Length of side 3 : "<<c<<endl;
         cout<<"Volume of the box : "<<vol<<endl;

     }


};

int main()
{
    double x,y,z;
    cout<<"Enter length of side 1 : ";
    cin>>x;
    cout<<"Enter length of side 2 : ";
    cin>>y;
    cout<<"Enter length of side 3 : ";
    cin>>z;
    box cub(x,y,z);
    cub.volume();
    cub.dis();
    return 0;
}
